#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
        exit 1
fi

echo && \
echo && \
echo "Running references" && \
echo && \
cd $ARTIFACT_DIR && \
./scripts/07-----generation.sh
echo && \
echo "Finished running references" && \
echo && \
echo
