#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
        exit 1
fi

echo && \
echo && \
echo "Running all experiments" && \
echo && \
cd $ARTIFACT_DIR && \
./scripts/03-----generation.sh
./scripts/04-----storage.sh
./scripts/05-----exploration.sh
echo && \
echo "Finished running all experiments" && \
echo && \
echo
