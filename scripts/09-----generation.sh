#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
	exit 1
fi

cd $ARTIFACT_DIR && \
gnuplot scripts/internal/gnuplot_generation && \
echo "Successfully generated generation plot"
