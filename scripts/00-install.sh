#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
	exit 1
fi

python -c "import opentuner"
if [ $? -ne 0 ]
then
	echo "OpenTuner is not installed. Attempt installation? (y/n)"
	read -n 1 answer
	if [ "$answer" == "y" ]
	then
		echo
		echo "Trying to install OpenTuner"
		apt-get install sqlite3 libsqlite3-dev && \
		pip install opentuner
		if [ $? -eq 0 ]; then
			echo "... successful"
		else
			exit 1
		fi
	else
		echo
		echo "Please install OpenTuner before using this artifact"
		exit 1
	fi
fi

cd $ARTIFACT_DIR && \
cd build && \
rm -rf * && \
cmake -DCMAKE_BUILD_TYPE="Release" ${@:1} .. && \
make -j `nproc` && \
echo && \
echo "Artifact installation successful!"
