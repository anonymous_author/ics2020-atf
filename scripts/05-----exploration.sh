#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
        exit 1
fi

echo && \
echo && \
echo "Running exploration experiments" && \
echo && \
cd $ARTIFACT_DIR/build/exploration && \
platform_id=`jq '.platform_id' ../../config.json` && \
device_id=`jq '.device_id' ../../config.json` && \
exploration_runs=`jq '.exploration."exploration runs"' ../../config.json` && \
for run in `seq 1 $exploration_runs`
do
  for app in "Gaussian" "GEMM" "CCSD(T)" "PRL"
  do
    [ "$app" = "Gaussian" ] && exec_name="gaussian"
    [ "$app" = "GEMM" ] && exec_name="gemm"
    [ "$app" = "CCSD(T)" ] && exec_name="tc"
    [ "$app" = "PRL" ] && exec_name="prl"
    size=`jq --raw-output ".exploration.\"$app\".\"input size\" | join(\" \")" ../../config.json` && \
    minutes=`jq --raw-output ".exploration.\"$app\".\"exploration minutes per run\"" ../../config.json` && \
    ./exploration_sa_flat_$exec_name $platform_id $device_id $minutes $size
    if [ $? -ne 0 ]; then
      exit 1
    fi
    ./exploration_sa_tree_$exec_name $platform_id $device_id $minutes $size
    if [ $? -ne 0 ]; then
      exit 1
    fi
    ./exploration_ot_flat_$exec_name $platform_id $device_id $minutes $size
    if [ $? -ne 0 ]; then
      exit 1
    fi
    ./exploration_ot_tree_$exec_name $platform_id $device_id $minutes $size
    if [ $? -ne 0 ]; then
      exit 1
    fi
  done
done
echo && \
echo "Finished running exploration experiments" && \
echo && \
echo
