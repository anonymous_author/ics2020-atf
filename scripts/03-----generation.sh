#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
        exit 1
fi

echo && \
echo && \
echo "Running generation experiment" && \
echo && \
cd $ARTIFACT_DIR/build/generation && \
platform_id=`jq '.platform_id' ../../config.json` && \
device_id=`jq '.device_id' ../../config.json` && \
for app in `jq --raw-output '.generation.applications | .[]' ../../config.json`
do
  for size in `jq --raw-output '.generation."input sizes" | .[] | join("-")' ../../config.json`
  do
    [ "$app" = "Gaussian" ] && exec_name="gaussian"
    [ "$app" = "GEMM" ] && exec_name="gemm"
    [ "$app" = "CCSD(T)" ] && exec_name="tc"
    [ "$app" = "PRL" ] && exec_name="prl"
    size=`echo "\"$size\"" | jq --raw-output 'split("-") | join(" ")'` && \
    ./generation_$exec_name $platform_id $device_id $size
    if [ $? -ne 0 ]; then
      exit 1
    fi
  done
done
echo && \
echo "Finished running generation experiment" && \
echo && \
echo
