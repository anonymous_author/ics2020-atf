import csv
import os.path

for app in ["gaussian", "gemm", "tc", "prl"]:
    data = [["#", "SA (ATF)", "SA (CLTune / KernelTuner)", "AUC (ATF)", "AUC (CLTune / KernelTuner)"]]
    if os.path.isfile("build/exploration/exploration_sa_flat_{}.csv".format(app)):
        with open("build/exploration/exploration_sa_flat_{}.csv".format(app), "r") as sa_flat_file:
            sa_flat_reader = csv.reader(sa_flat_file, delimiter=";")
            next(sa_flat_reader)
            i=0
            for row in sa_flat_reader:
                if i == len(data) - 1:
                    data.append([None, None, None, None])
                data[i+1][0] = str(float(row[1]) * 1000)
                i=i+1
    if os.path.isfile("build/exploration/exploration_sa_tree_{}.csv".format(app)):
        with open("build/exploration/exploration_sa_tree_{}.csv".format(app), "r") as sa_tree_file:
            sa_tree_reader = csv.reader(sa_tree_file, delimiter=";")
            next(sa_tree_reader)
            i=0
            for row in sa_tree_reader:
                if i == len(data) - 1:
                    data.append([None, None, None, None])
                data[i+1][1] = str(float(row[1]) * 1000)
                i=i+1
    if os.path.isfile("build/exploration/exploration_ot_flat_{}.csv".format(app)):
        with open("build/exploration/exploration_ot_flat_{}.csv".format(app), "r") as ot_flat_file:
            ot_flat_reader = csv.reader(ot_flat_file, delimiter=";")
            next(ot_flat_reader)
            i=0
            for row in ot_flat_reader:
                if i == len(data) - 1:
                    data.append([None, None, None, None])
                data[i+1][2] = str(float(row[1]) * 1000)
                i=i+1
    if os.path.isfile("build/exploration/exploration_ot_tree_{}.csv".format(app)):
        with open("build/exploration/exploration_ot_tree_{}.csv".format(app), "r") as ot_tree_file:
            ot_tree_reader = csv.reader(ot_tree_file, delimiter=";")
            next(ot_tree_reader)
            i=0
            for row in ot_tree_reader:
                if i == len(data) - 1:
                    data.append([None, None, None, None])
                data[i+1][3] = str(float(row[1]) * 1000)
                i=i+1

    with open("build/exploration/exploration_boxplots_{}.csv".format(app), "w") as out_file:
        for row in data:
            if None in row:
                break
            out_file.write(";".join(row))
            out_file.write("\n")