#!/bin/bash

: ${ARTIFACT_DIR?"Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"}
if [ -z "$ARTIFACT_DIR" ]
then
    	echo "Please set the environment variable ARTIFACT_DIR to the root dir of the artifact (the directory containing the scripts folder)"
	exit 1
fi

cd $ARTIFACT_DIR && \
./scripts/09-----generation.sh
./scripts/10-----storage.sh
./scripts/11-----exploration.sh
echo && \
echo && \
echo "Plotting successful!"
