#include <fstream>
#include <iostream>
#include <vector>

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#undef  __CL_ENABLE_EXCEPTIONS

#include "include/picojson.h"

#include <unistd.h>
#include <fcntl.h>
#include <fstream>

namespace artifact {

void check_error(cl_int err) {
    if (err != CL_SUCCESS) {
        printf("OpenCL error with errorcode: %d\n", err);
        throw std::exception();
    }
}

cl::Device get_device(int platform_id, int device_id, bool silent = false) {
    std::vector<cl::Platform> platforms;
    auto error = cl::Platform::get(&platforms); check_error(error);
    if (platform_id >= platforms.size()) {
        std::cout << "No platform with id " << platform_id << std::endl;
        exit(1);
    }

    cl::Platform p = platforms[platform_id];
    std::string platform_name;
    p.getInfo(CL_PLATFORM_VENDOR, &platform_name);
    if (!silent) std::cout << "Platform with name " << platform_name << " found." << std::endl;

    std::vector<cl::Device> devices;
    error = p.getDevices(CL_DEVICE_TYPE_ALL, &devices); check_error(error);
    if (device_id >= devices.size()) {
        std::cout << "No device with id " << device_id << " for platform with id " << platform_id << std::endl;
        exit(1);
    }

    cl::Device d = devices[device_id];
    std::string device_name;
    d.getInfo(CL_DEVICE_NAME, &device_name);
    if (!silent) std::cout << "Device with name " << device_name << " found." << std::endl;
    return d;
}

void append_to_file_exclusively(const std::string& file, const std::string& data, const std::string& init = "") {
    // open file
    int fd = open(file.c_str(), O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (fd == -1) {
        std::cerr << "error while opening file " << file << std::endl;
        std::cerr << "errno: " << errno << std::endl;
        exit(1);
    }

    // lock
    struct flock fl = {};
    memset(&fl, 0, sizeof(fl));
    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;
    if (fcntl(fd, F_SETLKW, &fl) != 0) {
        std::cerr << "error while getting lock for file " << file << std::endl;
        std::cerr << "errno: " << errno << std::endl;
        exit(1);
    }

    // check if initialized
    auto file_length = lseek(fd, 0, SEEK_END);
    if (file_length < 0) {
        std::cerr << "error while getting file length for file " << file << std::endl;
        std::cerr << "errno: " << errno << std::endl;
        exit(1);
    }
    if (!init.empty() && file_length == 0) {
        if (write(fd, init.c_str(), init.length()) != init.length()) {
            std::cerr << "error while writing initial string to file " << file << std::endl;
            std::cerr << "errno: " << errno << std::endl;
            exit(1);
        }
    }

    // write data
    if (write(fd, data.c_str(), data.length()) != data.length()) {
        std::cerr << "error while writing data string to file " << file << std::endl;
        std::cerr << "errno: " << errno << std::endl;
        exit(1);
    }

    if (close(fd) != 0) {
        std::cerr << "error while closing file " << file << std::endl;
        std::cerr << "errno: " << errno << std::endl;
        exit(1);
    }
}

}
