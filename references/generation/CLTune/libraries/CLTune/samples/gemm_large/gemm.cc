
// =================================================================================================
// This file is part of the CLTune project, which loosely follows the Google C++ styleguide and uses
// a tab-size of two spaces and a max-width of 100 characters per line.
//
// Author: cedric.nugteren@surfsara.nl (Cedric Nugteren)
//
// This file demonstrates the usage of CLTune with a more advanced matrix-multiplication example.
// This matrix-matrix multiplication is also heavily tuned and competes performance-wise with the
// clBLAS library.
// In contrast to the regular 'gemm' example, the 'gemm_search_methods' example searchers through a
// much larger parameter space, but uses smart search techniques instead of full search. Examples
// are simulated annealing (the default) and particle swarm optimisation (see below).
//
// -------------------------------------------------------------------------------------------------
//
// Copyright 2014 SURFsara
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// =================================================================================================

// include artifact settings
#include "../../../../../config.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <chrono>
#include <random>

// Includes the OpenCL tuner library
#include "cltune.h"

// Helper function to determine whether or not 'a' is a multiple of 'b'
bool IsMultiple(size_t a, size_t b) {
  return ((a/b)*b == a) ? true : false;
};

// =================================================================================================

// Example showing how to tune an OpenCL SGEMM matrix-multiplication kernel. This assumes that
// matrix B is pre-transposed, alpha equals 1 and beta equals 0: C = A * B^T
int main(int argc, char* argv[]) {
  int pot = -1;
  if (argc >= 2) {
    try {
      pot = std::stoi(argv[1]);
    } catch(std::invalid_argument &e) {
      pot = -1;
    }
  }
  if (pot < 0) {
    std::cout << "usage: " << argv[0] << " (pot)" << std::endl;
    std::cout << "measures the execution time for CLTune for the search space generation for the input size 2^pot" << std::endl;
    exit(1);
  }

  std::ofstream csv_file;
  csv_file.open("cltune_generation.csv", std::ofstream::out | std::ofstream::app);
  if (!csv_file.is_open()) {
    std::cout << "Could not open output file!" << std::endl;
  }
  if (csv_file.tellp() == 0) {
    csv_file << "#input_size;runtime" << std::endl;
  }  

  // Settings (sizes)
  const size_t kSizeM = std::pow(2, pot);
  const size_t kSizeN = std::pow(2, pot);
  const size_t kSizeK = std::pow(2, pot);

  std::vector<size_t> m_values(kSizeM);
  for (size_t i = 1; i <= kSizeM; ++i) m_values[i - 1] = i;
  std::vector<size_t> n_values(kSizeN);
  for (size_t i = 1; i <= kSizeN; ++i) n_values[i - 1] = i;
  std::vector<size_t> k_values(kSizeK);
  for (size_t i = 1; i <= kSizeK; ++i) k_values[i - 1] = i;

  // Sets the filenames of the OpenCL kernels (optionally automatically translated to CUDA)
  auto gemm_fast = std::vector<std::string>{"../../../../references/generation/CLTune/samples/gemm/gemm.opencl"};
  auto gemm_reference = std::vector<std::string>{"../../../../references/generation/CLTune/samples/gemm/gemm_reference.opencl"};
  #ifndef USE_OPENCL
    gemm_fast.insert(gemm_fast.begin(), "../../../../references/generation/CLTune/samples/cl_to_cuda.h");
    gemm_reference.insert(gemm_reference.begin(), "../../../../references/generation/CLTune/samples/cl_to_cuda.h");
  #endif

  // Selects the device, the search method and its first parameter. These parameters are all
  // optional and are thus also given default values.
  auto method = 1;
  auto search_param_1 = 4;

  // Creates input matrices
  auto mat_a = std::vector<float>(kSizeM*kSizeK);
  auto mat_b = std::vector<float>(kSizeN*kSizeK);
  auto mat_c = std::vector<float>(kSizeM*kSizeN);

  // Create a random number generator
  const auto random_seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(static_cast<unsigned int>(random_seed));
  std::uniform_real_distribution<float> distribution(-2.0f, 2.0f);

  // Populates input data structures
  for (auto &item: mat_a) { item = distribution(generator); }
  for (auto &item: mat_b) { item = distribution(generator); }
  for (auto &item: mat_c) { item = 0.0f; }

  // Initializes the tuner (platform 0, device 'device_id')
  cltune::Tuner tuner(artifact::get_platform_id(), artifact::get_device_id());
  tuner.SetNumRuns(5);

  // Sets one of the following search methods:
  // 0) Random search
  // 1) Simulated annealing
  // 2) Particle swarm optimisation (PSO)
  // 3) Full search
//  auto fraction = 1.0f/2048.0f;
  auto fraction = 0.0f; // do not tune, only generate search space
  if      (method == 0) { tuner.UseRandomSearch(fraction); }
  else if (method == 1) { tuner.UseAnnealing(fraction, static_cast<double>(search_param_1)); }
  else if (method == 2) { tuner.UsePSO(fraction, static_cast<size_t>(search_param_1), 0.4, 0.0, 0.4); }
  else                  { tuner.UseFullSearch(); }

  // Outputs the search process to a file
  tuner.OutputSearchLog("search_log.txt");
  
  // ===============================================================================================

  // Adds a heavily tuneable kernel and some example parameter values. Others can be added, but for
  // this example this already leads to plenty of kernels to test.
  auto id = tuner.AddKernel(gemm_fast, "gemm_fast", {kSizeM, kSizeN}, {1, 1});
  tuner.AddParameter(id, "MWG", m_values);
  tuner.AddParameter(id, "NWG", n_values);
  tuner.AddParameter(id, "KWG", k_values);
  tuner.AddParameter(id, "MDIMC", m_values);
  tuner.AddParameter(id, "NDIMC", n_values);
  tuner.AddParameter(id, "MDIMA", m_values);
  tuner.AddParameter(id, "NDIMB", n_values);
  tuner.AddParameter(id, "KWI", k_values);
  tuner.AddParameter(id, "VWM", {1, 2, 4, 8});
  tuner.AddParameter(id, "VWN", {1, 2, 4, 8});
  tuner.AddParameter(id, "STRM", {0, 1});
  tuner.AddParameter(id, "STRN", {0, 1});
  tuner.AddParameter(id, "SA", {0, 1});
  tuner.AddParameter(id, "SB", {0, 1});

  // Tests single precision (SGEMM)
  tuner.AddParameter(id, "PRECISION", {32});

  // Additional constraints that are required because of larger intervals for the tuning parameters
  auto DividesXMulY = [] (std::vector<size_t> v) { return IsMultiple(v[1]*v[2], v[0]); };
  auto DividesM = [=] (std::vector<size_t> v) { return IsMultiple(kSizeM, v[0]); };
  auto DividesN = [=] (std::vector<size_t> v) { return IsMultiple(kSizeN, v[0]); };
  auto DividesK = [=] (std::vector<size_t> v) { return IsMultiple(kSizeK, v[0]); };
  tuner.AddConstraint(id, DividesM, {"MWG"});
  tuner.AddConstraint(id, DividesN, {"NWG"});
  tuner.AddConstraint(id, DividesK, {"KWG"});
  tuner.AddConstraint(id, DividesXMulY, {"MDIMA", "MDIMC", "NDIMC"});
  tuner.AddConstraint(id, DividesXMulY, {"NDIMB", "MDIMC", "NDIMC"});

  // Sets constraints: Set-up the constraints functions to use. The constraints require a function
  // object (in this case a lambda) which takes a vector of tuning parameter values and returns
  // a boolean value whether or not the tuning configuration is legal. In this case, the helper
  // function 'IsMultiple' is employed for convenience. In the calls to 'AddConstraint' below, the
  // vector of parameter names (as strings) matches the input integer vector of the lambda's.
  auto MultipleOfX = [] (std::vector<size_t> v) { return IsMultiple(v[0], v[1]); };
  auto MultipleOfXMulY = [] (std::vector<size_t> v) { return IsMultiple(v[0], v[1]*v[2]); };
  auto MultipleOfXMulYDivZ = [] (std::vector<size_t> v) { return IsMultiple(v[0], (v[1]*v[2])/v[3]); };

  // Sets constraints: Requirement for unrolling the KWG loop
  tuner.AddConstraint(id, MultipleOfX, {"KWG", "KWI"});

  // Sets constraints: Required for integer MWI and NWI
  tuner.AddConstraint(id, MultipleOfXMulY, {"MWG", "MDIMC", "VWM"});
  tuner.AddConstraint(id, MultipleOfXMulY, {"NWG", "NDIMC", "VWN"});

  // Sets constraints: Required for integer MWIA and NWIB
  tuner.AddConstraint(id, MultipleOfXMulY, {"MWG", "MDIMA", "VWM"});
  tuner.AddConstraint(id, MultipleOfXMulY, {"NWG", "NDIMB", "VWN"});

  // Sets constraints: KWG has to be a multiple of KDIMA = ((MDIMC*NDIMC)/(MDIMA)) and KDIMB = (...)
  tuner.AddConstraint(id, MultipleOfXMulYDivZ, {"KWG", "MDIMC", "NDIMC", "MDIMA"});
  tuner.AddConstraint(id, MultipleOfXMulYDivZ, {"KWG", "MDIMC", "NDIMC", "NDIMB"});

  // Sets the constraints for local memory size limitations
  auto LocalMemorySize = [] (std::vector<size_t> v) {
    return (((v[0]*v[1]*v[2]/v[3]) + (v[4]*v[5]*v[6]/v[7]))*sizeof(float));
  };
  tuner.SetLocalMemoryUsage(id, LocalMemorySize, {"SA", "KWG", "MWG", "VWM", "SB", "KWG", "NWG", "VWN"});

  // Modifies the thread-sizes (both global and local) based on the parameters
  tuner.MulLocalSize(id, {"MDIMC", "NDIMC"});
  tuner.MulGlobalSize(id, {"MDIMC", "NDIMC"});
  tuner.DivGlobalSize(id, {"MWG", "NWG"});

  // ===============================================================================================

  // Sets the tuner's golden reference function. This kernel contains the reference code to which
  // the output is compared. Supplying such a function is not required, but it is necessarily for
  // correctness checks to be enabled.
//  tuner.SetReference(gemm_reference, "gemm_reference", {kSizeM, kSizeN}, {8,8});

  // Sets the function's arguments. Note that all kernels have to accept (but not necessarily use)
  // all input arguments.
  tuner.AddArgumentScalar(static_cast<int>(kSizeM));
  tuner.AddArgumentScalar(static_cast<int>(kSizeN));
  tuner.AddArgumentScalar(static_cast<int>(kSizeK));
  tuner.AddArgumentInput(mat_a);
  tuner.AddArgumentInput(mat_b);
  tuner.AddArgumentOutput(mat_c);

  // Starts the tuner
  csv_file << std::pow(2, pot) << ";";
  tuner.Tune(&csv_file);
  csv_file << std::endl;

  // Prints the results to screen and to file
  auto time_ms = tuner.PrintToScreen();
  tuner.PrintToFile("output.csv");
//  tuner.PrintFormatted();

  // Also prints the performance of the best-case in terms of GFLOPS
  const auto kMGFLOP = (2*kSizeM*kSizeN*kSizeK) * 1.0e-6;
  if (time_ms != 0.0) {
//    printf("[ -------> ] %.1lf ms or %.3lf GFLOPS\n", time_ms, kMGFLOP/time_ms);
  }

  // End of the tuner example
  return 0;
}

// =================================================================================================
