//
//  coordinate_space.hpp
//  new_atf_lib
//
//

#ifndef coordinate_space_h
#define coordinate_space_h

#include <cmath>
#include <random>
#include <chrono>
#include <algorithm>

#include "search_space.hpp"


namespace atf
{

class point
{

public:
  using iterator = std::vector<double>::iterator;
  using const_iterator = std::vector<double>::const_iterator;

  explicit point(std::size_t size)
    : _coords(size)
  {
  }

  template <typename... Ts>
  explicit point( Ts... coords )
    : _coords( coords... )
  {}

  explicit point( const std::vector<double>& coords )
    : _coords( coords )
  {}


  size_t dimension() const
  {
    return _coords.size();
  }

  double& operator[]( size_t i )
  {
      return _coords[ i ];
  }

  double operator[]( size_t i ) const
  {
    return _coords[ i ];
  }

  iterator begin()
  {
    return _coords.begin();
  }

  iterator end()
  {
    return _coords.end();
  }

  const_iterator begin() const
  {
    return _coords.begin();
  }

  const_iterator end() const
  {
    return _coords.end();
  }


  point operator*( double a ) const
  {
    std::vector<double> r( dimension() );
    for( size_t i = 0 ; i < dimension() ; i++ )
      r[ i ] = a * _coords[ i ];

    return point( r );
  }

  point operator*(const point& p) const
  {
    std::vector<double> r(dimension());
    std::generate(r.begin(), r.end(), [&, i = 0] () mutable {
      auto tmp_i = i++;
      return (*this)[tmp_i] * p[tmp_i];
    });

    return point(r);
  }


  point operator+( const point& p ) const
  {
    if( p.dimension() != dimension() )
      throw std::invalid_argument( "Dimensions of points do not match." );

    std::vector<double> r( dimension() );
    for( size_t i = 0 ; i < dimension() ; i++ )
      r[ i ] = _coords[ i ] + p[ i ];

    return point( r );
  }


  point operator-( const point& p ) const
  {
    return this->operator+( p * -1 );
  }

  point& operator+=(const point& p)
  {
    for (std::size_t i = 0; i < dimension(); ++i) {
      _coords[i] += p[i];
    }

    return *this;
  }

  point& operator-=(const point& p)
  {
    for (std::size_t i = 0; i < dimension(); ++i) {
      _coords[i] -= p[i];
    }

    return *this;
  }

  void push_back(double a){
    _coords.push_back(a);
  }

  void replace(size_t i, double a)
  {
    this->_coords[ i ] = a;
  }

private:
  std::vector<double> _coords;

};


class coordinate_space
{
public:
  coordinate_space()
    : _search_space()
  {}


  explicit coordinate_space( const search_space& search_space )
    : _search_space( &search_space )
  {
    _rng                = std::default_random_engine( random_seed() );
    _coord_distribution = std::uniform_real_distribution<double>( 0.0, 1.0 );
  }

  size_t dimension(){
    return _search_space->num_params();
  }

  point random_point()
  {
    std::vector<double> r( _search_space->num_params() );
    for( size_t i = 0 ; i < _search_space->num_params() ; i++ )
      r[ i ] = _coord_distribution( _rng );

    return point( r );
  }


  bool is_valid_point( const point& p ) const
  {
    if( p.dimension() != _search_space->num_params() )
      throw std::invalid_argument( "Dimension of point does not match number of parameters." );

    for( size_t i = 0 ; i < p.dimension() ; i++ )
    {
      if( p[ i ] < 0.0 || p[ i ] > 1.0 )
        return false;
    }

    return true;
  }


  point convert_to_valid_point_capped( const point& p ) const
  {
    if( p.dimension() != _search_space->num_params() )
      throw std::invalid_argument( "Dimension of point does not match number of parameters." );

    std::vector<double> r( p.dimension() );
    for( size_t i = 0 ; i < p.dimension() ; i++ )
      r[ i ] = std::max( 0.0, std::min( 1.0, p[ i ] ) );

    return point( r );
  }


  point convert_to_valid_point_periodic( const point& p ) const
  {
    if( p.dimension() != _search_space->num_params() )
      throw std::invalid_argument( "Dimension of point does not match number of parameters." );

    std::vector<double> r( p.dimension() );
    for( size_t i = 0 ; i < p.dimension() ; i++ )
    {
      if( p[ i ] < 0.0 || p[ i ] > 1.0 )
        r[ i ] = p[ i ] - std::floor( p[ i ] );
    }

    return point( r );
  }


  point convert_to_valid_point_mirrored( const point& p ) const
  {
    if( p.dimension() != _search_space->num_params() )
      throw std::invalid_argument( "Dimension of point does not match number of parameters." );

    std::vector<double> r( p.dimension() );
    for( size_t i = 0 ; i < p.dimension() ; i++ )
    {
      if( p[ i ] < 0.0 || p[ i ] > 1.0 )
      {
        double int_part;
        double frac_part = std::modf( p[ i ], &int_part );

        if( static_cast<int>( int_part ) % 2 == 0 )
          r[ i ] = std::abs( frac_part );
        else
          r[ i ] = 1.0 - std::abs( frac_part );
      }
    }

    return point( r );
  }

  point convert_to_valid_point_mod( const point& p ) const
  {
    if( p.dimension() != _search_space->num_params() )
      throw std::invalid_argument( "Dimension of point does not match number of parameters." );

    std::vector<double> r( p.dimension() );
    for( size_t i = 0 ; i < p.dimension() ; i++ ) {
      r[ i ] = fmod( std::fabs( p[ i ] ), 1 );
    }

    return point( r );
  }


  configuration get_config( const point& p ) const
  {
    if(p.dimension() != _search_space->num_params() )
      throw std::invalid_argument( "Dimension of point does not match number of parameters." );

    auto indices = get_raw_indices( p );
    // adapt indices to the actual number of children of the considered sub-tree
    for( size_t i = 0 ; i < indices.size() ; i++ )
    {
      auto ancestor_indices = indices;
      ancestor_indices.erase( ancestor_indices.begin() + i, ancestor_indices.begin() + ancestor_indices.size() );

      indices[ i ] %= _search_space->max_childs_of_node( ancestor_indices );
    }

    return _search_space->get_configuration( indices );
  }


private:
  search_space const* _search_space;

  std::default_random_engine             _rng;
  std::uniform_real_distribution<double> _coord_distribution;


  size_t random_seed() const
  {
    return static_cast<size_t>( std::chrono::system_clock::now().time_since_epoch().count() );
  }


  size_t coord_to_index( double coord, size_t param_index ) const
  {
    auto max_index = _search_space->max_childs( param_index ) - 1;

    return static_cast<size_t>( std::llround( coord * max_index ) );    // TODO: compare with OpenTuner approach
  }


    std::vector<size_t> get_raw_indices( const point& p ) const
    {
      if( p.dimension() != _search_space->num_params() )
        throw std::invalid_argument( "Dimension of point does not match number of parameters." );

      auto p_valid = convert_to_valid_point_capped( p );

      std::vector<size_t> indices( p_valid.dimension() );
      for( size_t i = 0 ; i < p_valid.dimension() ; i++ )
        indices[ i ] = coord_to_index( p_valid[ i ], i );

      return indices;
    }

};

} // namespace "atf"

#endif /* coordinate_space_h */
