//
// Created by  on 13.05.2017.
//

#ifndef MD_HOM_OCL_MD_HOM_WRAPPER_HPP
#define MD_HOM_OCL_MD_HOM_WRAPPER_HPP

#define SAVE_BINARY 0

#include <cstddef>
#include <tuple>
#include <chrono>

#include "ocl_wrapper.hpp"
#include "helper.hpp"

//#define __CL_ENABLE_EXCEPTIONS
#include "../libraries/cl.hpp"
//#undef  __CL_ENABLE_EXCEPTIONS

namespace atf {
namespace cf {
    template<typename GS_0_1, typename GS_1_1, typename GS_2_1,
            typename LS_0_1, typename LS_1_1, typename LS_2_1,
            typename GS_0_2, typename GS_1_2, typename GS_2_2,
            typename LS_0_2, typename LS_1_2, typename LS_2_2,
            typename INT_RES_T,
            typename... Ts>
    class ocl_md_hom_wrapper_class {
        // helper
        cl_int  error;
        cl_uint arg_index  = 0;
        size_t  buffer_pos = 0;

    public:
        ocl_md_hom_wrapper_class(const device_info&                        device,
                                 const std::tuple<Ts...>&                  kernel_inputs,
                                 const buffer_class<INT_RES_T>&            int_res_buffer,
                                 std::vector<std::string>                  wg_r_tp_names,
                                 bool (*is_valid)(configuration&),

                                 const kernel_info&                        kernel_1,
                                 const std::tuple<GS_0_1, GS_1_1, GS_2_1>& global_size_1,
                                 const std::tuple<LS_0_1, LS_1_1, LS_2_1>& local_size_1,
                                 const size_t                              num_inputs_1,

                                 const kernel_info&                        kernel_2,
                                 const std::tuple<GS_0_2, GS_1_2, GS_2_2>& global_size_2,
                                 const std::tuple<LS_0_2, LS_1_2, LS_2_2>& local_size_2,
                                 const size_t                              num_inputs_2,

                                 const size_t                              num_evaluations,
                                 const size_t                              warm_ups) :
                _platform( device.platform()), _device(device.device()), _context(), _command_queue(),
                _kernel_inputs(kernel_inputs), _int_res_buffer_data(int_res_buffer), _int_res_buffer(), _wg_r_tp_names(wg_r_tp_names), _is_valid(is_valid),
                _program_1(), _kernel_source_1(kernel_1.source()), _kernel_name_1(kernel_1.name()), _kernel_flags_1(kernel_1.flags()), _global_size_pattern_1(global_size_1), _local_size_pattern_1(local_size_1), _num_inputs_1(num_inputs_1), _kernel_buffers_1(), _kernel_input_sizes_1(),
                _program_2(), _kernel_source_2(kernel_2.source()), _kernel_name_2(kernel_2.name()), _kernel_flags_2(kernel_2.flags()), _global_size_pattern_2(global_size_2), _local_size_pattern_2(local_size_2), _num_inputs_2(num_inputs_2), _kernel_buffers_2(), _kernel_input_sizes_2(),
                _num_evaluations(num_evaluations), _warm_ups(warm_ups)
        {
            init_open_cl();

            std::cout << "_warm_ups: " << _warm_ups << std::endl;
            std::cout << "_num_evaluations: " << _num_evaluations << std::endl;
        }

        void init_open_cl() {
            // delete existing open cl objects
            _kernel_buffers_2.clear();
            _kernel_buffers_1.clear();
            _kernel_buffer_sizes_1.clear();
            _kernel_buffer_sizes_2.clear();
            _kernel_input_sizes_1.clear();
            _kernel_input_sizes_2.clear();

            // create context and command queue
            cl_context_properties props[] = { CL_CONTEXT_PLATFORM,
                                              reinterpret_cast<cl_context_properties>( _platform() ),
                                              0
            };

            _context       = cl::Context( VECTOR_CLASS<cl::Device>( 1, _device ), props );
            _command_queue = cl::CommandQueue( _context, _device, CL_QUEUE_PROFILING_ENABLE);

            // create programs
            _program_1 = cl::Program(_context,
                                     cl::Program::Sources( 1, std::make_pair( _kernel_source_1.c_str(), _kernel_source_1.length() ) )
            );
            _program_2 = cl::Program(_context,
                                     cl::Program::Sources( 1, std::make_pair( _kernel_source_2.c_str(), _kernel_source_2.length() ) )
            );

            // create kernel input buffers
            this->create_buffers(_kernel_buffers_1, _kernel_buffer_sizes_1, _kernel_input_sizes_1,             0, _num_inputs_1, std::make_index_sequence<sizeof...(Ts)>());
            this->create_buffers(_kernel_buffers_2, _kernel_buffer_sizes_2, _kernel_input_sizes_2, _num_inputs_1, _num_inputs_2, std::make_index_sequence<sizeof...(Ts)>());
        }

        bool check_results(atf::configuration &configuration,
                           const std::vector<void*> kernel_1_inputs, const std::vector<void*> kernel_1_golds,
                           void* const int_res_gold,
                           const std::vector<void*> kernel_2_inputs, const std::vector<void*> kernel_2_golds) {
            // copy inputs
            for (int i = 0; i < kernel_1_inputs.size(); ++i) {
                error = _command_queue.enqueueWriteBuffer(_kernel_buffers_1[i], CL_TRUE, 0, _kernel_buffer_sizes_1[i],
                                                          kernel_1_inputs[i]); check_error(error);
            }
            for (int i = 0; i < kernel_2_inputs.size(); ++i) {
                error = _command_queue.enqueueWriteBuffer(_kernel_buffers_2[i], CL_TRUE, 0, _kernel_buffer_sizes_2[i],
                                                          kernel_2_inputs[i]); check_error(error);
            }

            // run kernel with only 1 evaluation and no warm ups
            const size_t old_warm_ups = _warm_ups;
            const size_t old_num_evaluations = _num_evaluations;
            _warm_ups = 0;
            _num_evaluations = 1;
            (*this)(configuration);
            _warm_ups = old_warm_ups;
            _num_evaluations = old_num_evaluations;

            // check results
            bool errors = false;
            // first kernel
            for (int i = 0; i < kernel_1_inputs.size(); ++i) {
                void* dev_result = malloc(_kernel_buffer_sizes_1[i]);
                error = _command_queue.enqueueReadBuffer(_kernel_buffers_1[i], CL_TRUE, 0, _kernel_buffer_sizes_1[i],
                                                         dev_result); check_error(error);
                if (memcmp(dev_result, kernel_1_golds[i], _kernel_buffer_sizes_1[i])) {
                    std::cerr << "!!!ERROR IN RESULT OF FIRST KERNEL (BUFFER " << i << ")!!!" << std::endl;
                    for (int k = 0; k < _kernel_buffer_sizes_1[i] / sizeof(float); ++k) {
                        std::cout << ((float *) dev_result)[k] << "\t" << ((float*)kernel_1_golds[i])[k] << std::endl;
                    }
                    errors = true;
                }
                free(dev_result);
            }
            // int res
            // calculate size
            for (auto &tp : configuration) {
                auto tp_value = tp.second;
                tp_value.update_tp();
            }
            size_t num_wg_r = 1;
            for (auto& name : _wg_r_tp_names) {
                num_wg_r *= configuration[name].value().size_t_val();
            }
            size_t int_res_size = _int_res_buffer_data.size() * num_wg_r * sizeof(INT_RES_T);
            if (int_res_size > 0) {
                void *dev_result = malloc(int_res_size);
                error = _command_queue.enqueueReadBuffer(_int_res_buffer, CL_TRUE, 0, int_res_size,
                                                         dev_result);
                check_error(error);
                if (memcmp(dev_result, int_res_gold, int_res_size)) {
                    std::cerr << "!!!ERROR IN RESULT OF FIRST KERNEL (INTERMEDIATE BUFFER)!!!" << std::endl;
                    errors = true;
                }
                free(dev_result);
            }
            // second kernel
            for (int i = 0; i < kernel_2_inputs.size(); ++i) {
                void* dev_result = malloc(_kernel_buffer_sizes_2[i]);
                error = _command_queue.enqueueWriteBuffer(_kernel_buffers_2[i], CL_TRUE, 0, _kernel_buffer_sizes_2[i],
                                                          dev_result); check_error(error);
                if (memcmp(dev_result, kernel_2_golds[i], _kernel_buffer_sizes_2[i])) {
                    std::cerr << "!!!ERROR IN RESULT OF SECOND KERNEL (BUFFER " << i << ")!!!" << std::endl;
                    errors = true;
                }
                free(dev_result);
            }
            return !errors;
        }

        size_t operator()(atf::configuration &configuration, const std::vector<void*> &destinations, size_t *kernel_2_runtime) {
            read_final_buffers = true;
            _kernel_2_runtime = kernel_2_runtime;
            try {
                auto runtime = this->operator()(configuration);
                read_final_buffers = false;
                return runtime;
            } catch (cl::Error &err) {
                read_final_buffers = false;
                throw err;
            } catch (std::exception &ex) {
                read_final_buffers = false;
                throw ex;
            }
        }

        size_t operator()(configuration &configuration) {
            static size_t best_runtime = std::numeric_limits<size_t>::max() / _num_evaluations;

            if (!_is_valid(configuration)) throw std::exception();

            // update tp values
            for (auto &tp : configuration) {
                auto tp_value = tp.second;
                tp_value.update_tp();
//                std::cout << tp.first << "\t" << tp.second.value().size_t_val()<< std::endl;
            }

            // adapt size of intermediate results buffer
            // calculate new size
            size_t num_wg_r = 1;
            for (auto& name : _wg_r_tp_names) {
                num_wg_r *= configuration[name].value().size_t_val();
            }
            size_t new_size = _int_res_buffer_data.size() * num_wg_r * sizeof(INT_RES_T);
            if (new_size == 0) {
                // empty int_res buffer because it is not being used
                // set new_size to 1 to initially create the int_res buffer
                new_size = 1;
            }
            static size_t last_size = 0;
            if (new_size != last_size) {
                // replace buffer
                _int_res_buffer = cl::Buffer(_context, CL_MEM_READ_WRITE, new_size);
                last_size = new_size;
            }

            // ------------------------------
            // |          Kernel 1          |
            // ------------------------------

            // get global and local size for first kernel
            size_t gs_0_1 = std::get<0>(_global_size_pattern_1).get_value();
            size_t gs_1_1 = std::get<1>(_global_size_pattern_1).get_value();
            size_t gs_2_1 = std::get<2>(_global_size_pattern_1).get_value();

            size_t ls_0_1 = std::get<0>(_local_size_pattern_1).get_value();
            size_t ls_1_1 = std::get<1>(_local_size_pattern_1).get_value();
            size_t ls_2_1 = std::get<2>(_local_size_pattern_1).get_value();

//            printf("gs1: %lu, %lu, %lu; ls1: %lu, %lu, %lu\n", gs_0_1, gs_1_1, gs_2_1, ls_0_1, ls_1_1, ls_2_1);

            // create flags for first kernel
            std::stringstream flags_1;

            for (const auto &tp : configuration)
                flags_1 << " -D " << tp.second.name() << "=" << tp.second.value();

            // set additional kernel flags for first kernel
            flags_1 << _kernel_flags_1;

            // compile first kernel
            try {
                auto start = std::chrono::system_clock::now();
                _program_1.build(std::vector<cl::Device>(1, _device), flags_1.str().c_str());
                auto end = std::chrono::system_clock::now();

                auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
                std::cout << "compilation time for first kernel: " << runtime_in_sec << "ms" << std::endl;
            } catch (cl::Error &err) {
                std::cout << flags_1.str() << std::endl;
                std::cout << err.err() << std::endl;
                if (err.err() == CL_BUILD_PROGRAM_FAILURE) {
                    auto buildLog = _program_1.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device);
                    std::cout << std::endl << "Build of first kernel failed! Log:" << std::endl << buildLog << std::endl;
                }

                throw std::exception();
            }
            auto kernel_1 = cl::Kernel(_program_1, _kernel_name_1.c_str(), &error); check_error(error); // TODO: code aus PJS WS16/16 übernehmen für automatische Erkennung "func"

            // save program binary
#if ( SAVE_BINARY != 0 )  // Intel: objdump -M Intel -D -m i386 -b binary program_binary
            {
                size_t binary_size;

                // allocate memory
                clGetProgramInfo(_program_1(), CL_PROGRAM_BINARY_SIZES, sizeof(binary_size), &binary_size, NULL);
                char* binary = (char*) malloc( sizeof(char) * binary_size );

                // get binary
                clGetProgramInfo(_program_1(), CL_PROGRAM_BINARIES, sizeof(char) * binary_size , &binary, NULL);

                // save binary in file
                std::ofstream file;
                file.open("binary_program_1_kernel");
                file.write( binary, binary_size );
                file.close();

                // free memory
                free(binary);
            }
#endif

            // set kernel arguments for first kernel
            arg_index = 0;
            buffer_pos = 0;
            set_kernel_args(kernel_1, _kernel_buffers_1, 0, _kernel_input_sizes_1.size(), std::make_index_sequence<sizeof...(Ts)>());
            // add buffer for intermediate results as last argument
            kernel_1.setArg(arg_index++, _int_res_buffer);

            // start first kernel
            cl::Event event_1;
            cl::NDRange global_size_1(gs_0_1, gs_1_1, gs_2_1);
            cl::NDRange local_size_1(ls_0_1, ls_1_1, ls_2_1);

            // warm ups
            for (size_t i = 0; i < _warm_ups; ++i) {
                error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event_1); check_error(error);
            }
            if (_warm_ups > 0) {
                error = event_1.wait(); check_error(error);
            }

            // kernel launch with profiling
            cl_ulong kernel_runtime_in_ns_1 = 0;
            cl_ulong start_time_1;
            cl_ulong end_time_1;
            for (size_t i = 0; i < _num_evaluations; ++i) {
                error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event_1);
                error = event_1.wait(); check_error(error);

                error = event_1.getProfilingInfo(CL_PROFILING_COMMAND_START, &start_time_1); check_error(error);
                error = event_1.getProfilingInfo(CL_PROFILING_COMMAND_END, &end_time_1);     check_error(error);

                kernel_runtime_in_ns_1 += end_time_1 - start_time_1;

                if (kernel_runtime_in_ns_1 > best_runtime * _num_evaluations && !read_final_buffers) {
                    return kernel_runtime_in_ns_1 / (i + 1);
                }
            }
            kernel_runtime_in_ns_1 /= _num_evaluations;
            if (num_wg_r == 1) {
                if (read_final_buffers) *_kernel_2_runtime = 0;
                // return runtime of first kernel if second one is not needed
                if (kernel_runtime_in_ns_1 < best_runtime) best_runtime = kernel_runtime_in_ns_1;
                return kernel_runtime_in_ns_1;
            }

            // ------------------------------
            // |          Kernel 2          |
            // ------------------------------

            // get global and local size for second kernel
            size_t gs_0_2 = std::get<0>(_global_size_pattern_2).get_value();
            size_t gs_1_2 = std::get<1>(_global_size_pattern_2).get_value();
            size_t gs_2_2 = std::get<2>(_global_size_pattern_2).get_value();

            size_t ls_0_2 = std::get<0>(_local_size_pattern_2).get_value();
            size_t ls_1_2 = std::get<1>(_local_size_pattern_2).get_value();
            size_t ls_2_2 = std::get<2>(_local_size_pattern_2).get_value();

//            printf("gs2: %lu, %lu, %lu; ls2: %lu, %lu, %lu\n", gs_0_2, gs_1_2, gs_2_2, ls_0_2, ls_1_2, ls_2_2);

            // create flags for second kernel
            std::stringstream flags_2;

            for (const auto &tp : configuration)
                flags_2 << " -D " << tp.second.name() << "=" << tp.second.value();

            // set additional kernel flags for second kernel
            flags_2 << _kernel_flags_2;

            // compile second kernel
            try {
                auto start = std::chrono::system_clock::now();
                _program_2.build(std::vector<cl::Device>(1, _device), flags_2.str().c_str());
                auto end = std::chrono::system_clock::now();

                auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
                std::cout << "compilation time for second kernel: " << runtime_in_sec << "ms" << std::endl;
            } catch (cl::Error &err) {
                std::cout << flags_2.str() << std::endl;
                std::cout << err.err() << std::endl;
                if (err.err() == CL_BUILD_PROGRAM_FAILURE) {
                    auto buildLog = _program_2.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device);
                    std::cout << std::endl << "Build of second kernel failed! Log:" << std::endl << buildLog << std::endl;
                }

                throw std::exception();
            }
            auto kernel_2 = cl::Kernel(_program_2, _kernel_name_2.c_str(), &error); check_error(error); // TODO: code aus PJS WS16/16 übernehmen für automatische Erkennung "func"

            // save program binary
#if ( SAVE_BINARY != 0 )  // Intel: objdump -M Intel -D -m i386 -b binary program_binary
            {
                size_t binary_size;

                // allocate memory
                clGetProgramInfo(_program_2(), CL_PROGRAM_BINARY_SIZES, sizeof(binary_size), &binary_size, NULL);
                char* binary = (char*) malloc( sizeof(char) * binary_size );

                // get binary
                clGetProgramInfo(_program_2(), CL_PROGRAM_BINARIES, sizeof(char) * binary_size , &binary, NULL);

                // save binary in file
                std::ofstream file;
                file.open("binary_program_2_kernel");
                file.write( binary, binary_size );
                file.close();

                // free memory
                free(binary);
            }
#endif

            // set kernel arguments for second kernel
            arg_index = 0;
            buffer_pos = 0;
            // add buffer for intermediate results as first argument
            kernel_2.setArg(arg_index++, _int_res_buffer);
            set_kernel_args(kernel_2, _kernel_buffers_2, _kernel_input_sizes_1.size(), _kernel_input_sizes_2.size(), std::make_index_sequence<sizeof...(Ts)>());

            // start second kernel
            cl::Event event_2;
            cl::NDRange global_size_2(gs_0_2, gs_1_2, gs_2_2);
            cl::NDRange local_size_2(ls_0_2, ls_1_2, ls_2_2);

            // warm ups
            for (size_t i = 0; i < _warm_ups; ++i) {
                error = _command_queue.enqueueNDRangeKernel(kernel_2, cl::NullRange, global_size_2, local_size_2, NULL, &event_2); check_error(error);
            }
            if (_warm_ups > 0) {
                error = event_2.wait(); check_error(error);
            }

            // kernel launch with profiling
            cl_ulong kernel_runtime_in_ns_2 = 0;
            cl_ulong start_time_2;
            cl_ulong end_time_2;
            for (size_t i = 0; i < _num_evaluations; ++i) {
                error = _command_queue.enqueueNDRangeKernel(kernel_2, cl::NullRange, global_size_2, local_size_2, NULL, &event_2); check_error(error);

                error = event_2.wait(); check_error(error);

                error = event_2.getProfilingInfo(CL_PROFILING_COMMAND_START, &start_time_2); check_error(error);
                error = event_2.getProfilingInfo(CL_PROFILING_COMMAND_END, &end_time_2);     check_error(error);

                kernel_runtime_in_ns_2 += end_time_2 - start_time_2;

                if (kernel_runtime_in_ns_2 > (best_runtime - kernel_runtime_in_ns_1) * _num_evaluations && !read_final_buffers) {
                    return kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2 / (i + 1);
                }
            }
            kernel_runtime_in_ns_2 /= _num_evaluations;
            if (read_final_buffers) *_kernel_2_runtime = kernel_runtime_in_ns_2;
            if (kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2 < best_runtime)
                best_runtime = kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2;
            return kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2;
        }

    private:
        bool (*_is_valid)(configuration&);

        cl::Platform                        _platform;
        cl::Device                          _device;
        cl::Context                         _context;
        cl::CommandQueue                    _command_queue;

        std::tuple<Ts...>                   _kernel_inputs;
        const buffer_class<INT_RES_T>&      _int_res_buffer_data;
        cl::Buffer                          _int_res_buffer;
        std::vector<std::string>            _wg_r_tp_names;

        // Kernel 1
        cl::Program                         _program_1;
        std::string                         _kernel_source_1;
        std::string                         _kernel_name_1;
        std::string                         _kernel_flags_1;
        std::tuple<GS_0_1, GS_1_1, GS_2_1>  _global_size_pattern_1;
        std::tuple<LS_0_1, LS_1_1, LS_2_1>  _local_size_pattern_1;
        size_t                              _num_inputs_1;
        std::vector<cl::Buffer>             _kernel_buffers_1;
        std::vector<size_t>                 _kernel_buffer_sizes_1;
        std::vector<size_t>                 _kernel_input_sizes_1;

        // Kernel 2
        cl::Program                         _program_2;
        std::string                         _kernel_source_2;
        std::string                         _kernel_name_2;
        std::string                         _kernel_flags_2;
        std::tuple<GS_0_2, GS_1_2, GS_2_2>  _global_size_pattern_2;
        std::tuple<LS_0_2, LS_1_2, LS_2_2>  _local_size_pattern_2;
        size_t                              _num_inputs_2;
        std::vector<cl::Buffer>             _kernel_buffers_2;
        std::vector<size_t>                 _kernel_buffer_sizes_2;
        std::vector<size_t>                 _kernel_input_sizes_2;

        size_t                              _num_evaluations;
        size_t                              _warm_ups;

        // helper variables
        bool read_final_buffers = false;
        size_t* _kernel_2_runtime = nullptr;

        // helper for creating buffers
        template< size_t... Is>
        void create_buffers(std::vector<cl::Buffer>& _kernel_buffers,
                            std::vector<size_t>& _kernel_buffer_sizes,
                            std::vector<size_t>& _kernel_input_sizes,
                            size_t offset, size_t count,
                            std::index_sequence<Is...>) {
            create_buffers_impl(_kernel_buffers,
                                _kernel_buffer_sizes,
                                _kernel_input_sizes,
                                offset, count,
                                std::get<Is>(_kernel_inputs)...);
        }

        template<typename T, typename... ARGs>
        void create_buffers_impl(std::vector<cl::Buffer>& _kernel_buffers,
                                 std::vector<size_t>& _kernel_buffer_sizes,
                                 std::vector<size_t>& _kernel_input_sizes,
                                 size_t offset, size_t count,
                                 const scalar<T> &scalar, ARGs&... args) {
            if (offset == 0) {
                _kernel_input_sizes.emplace_back(1);
                if (count > 1) {
                    create_buffers_impl(_kernel_buffers,
                                        _kernel_buffer_sizes,
                                        _kernel_input_sizes,
                                        offset, count - 1,
                                        args...);
                }
            } else {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_buffer_sizes,
                                    _kernel_input_sizes,
                                    offset - 1, count,
                                    args...);
            }
        }

        template<typename T, typename... ARGs>
        void create_buffers_impl(std::vector<cl::Buffer>& _kernel_buffers,
                                 std::vector<size_t>& _kernel_buffer_sizes,
                                 std::vector<size_t>& _kernel_input_sizes,
                                 size_t offset, size_t count,
                                 const buffer_class<T> &buffer, ARGs&... args) {
            if (offset == 0) {
                auto start_time = std::chrono::system_clock::now();

                // add buffer size to _kernel_input_sizes
                _kernel_input_sizes.emplace_back(buffer.size());
                _kernel_buffer_sizes.emplace_back(buffer.size() * sizeof(T));

                // create buffer
                _kernel_buffers.emplace_back(_context, CL_MEM_READ_WRITE, buffer.size() * sizeof(T));

                try {
                    error = _command_queue.enqueueWriteBuffer(_kernel_buffers.back(), CL_TRUE,
                                                              0, _kernel_buffer_sizes.back(),
                                                              buffer.get());
                    check_error(error);
                }
                catch (cl::Error &err) {
                    std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")" << std::endl;
                    abort();
                }

                auto end_time = std::chrono::system_clock::now();
                auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
                std::cout << "Time to create and fill buffer: " << runtime << "ms" << std::endl;

                if (count > 1) {
                    create_buffers_impl(_kernel_buffers,
                                        _kernel_buffer_sizes,
                                        _kernel_input_sizes,
                                        offset, count - 1,
                                        args...);
                }
            } else {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_buffer_sizes,
                                    _kernel_input_sizes,
                                    offset - 1, count,
                                    args...);
            }
        }

        template<typename... ARGs>
        void create_buffers_impl(std::vector<cl::Buffer>& _kernel_buffers,
                                 std::vector<size_t>& _kernel_buffer_sizes,
                                 std::vector<size_t>& _kernel_input_sizes,
                                 size_t offset, size_t count,
                                 const cl_mem &ocl_buffer, ARGs &... args) {
            if (offset == 0) {
                // get buffer size
                size_t buffer_size;
                clGetMemObjectInfo(ocl_buffer, CL_MEM_SIZE, sizeof(size_t), &buffer_size, NULL);

                _kernel_input_sizes.emplace_back(buffer_size);
                _kernel_buffer_sizes.emplace_back(buffer_size);

                // set buffer
                _kernel_buffers.push_back(cl::Buffer(ocl_buffer));
                if (count > 1) {
                    create_buffers_impl(_kernel_buffers,
                                        _kernel_buffer_sizes,
                                        _kernel_input_sizes,
                                        offset, count - 1,
                                        args...);
                }
            } else {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_buffer_sizes,
                                    _kernel_input_sizes,
                                    offset - 1, count,
                                    args...);
            }
        }

        void create_buffers_impl(std::vector<cl::Buffer>& _kernel_buffers,
                                 std::vector<size_t>& _kernel_buffer_sizes,
                                 std::vector<size_t>& _kernel_input_sizes,
                                 size_t offset, size_t count) {}


        // helper for set kernel arguments
        template<size_t... Is>
        void set_kernel_args(cl::Kernel &kernel, std::vector<cl::Buffer>& _kernel_buffers, size_t offset, size_t count, std::index_sequence<Is...>) {
            set_kernel_args_impl(kernel, _kernel_buffers, offset, count, std::get<Is>(_kernel_inputs)...);
        }

        template<typename T, typename... ARGs>
        void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer>& _kernel_buffers, size_t offset, size_t count, scalar<T> scalar, ARGs... args) {
            if (offset == 0) {
                kernel.setArg(arg_index++, scalar.get());

                if (count > 1) {
                    set_kernel_args_impl(kernel, _kernel_buffers, offset, count - 1, args...);
                }
            } else {
                set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
            }
        }

        template<typename T, typename... ARGs>
        void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer>& _kernel_buffers, size_t offset, size_t count, buffer_class<T> buffer, ARGs... args) {
            if (offset == 0) {
                kernel.setArg(arg_index++, _kernel_buffers[buffer_pos++]);

                if (count > 1) {
                    set_kernel_args_impl(kernel, _kernel_buffers, offset, count - 1, args...);
                }
            } else {
                set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
            }
        }

        template<typename... ARGs>
        void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer>& _kernel_buffers, size_t offset, size_t count, cl_mem buffer, ARGs... args) {
            if (offset == 0) {
                kernel.setArg(arg_index++, _kernel_buffers[buffer_pos++]);

                if (count > 1) {
                    set_kernel_args_impl(kernel, _kernel_buffers, offset, count - 1, args...);
                }
            } else {
                set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
            }
        }

        void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer>& _kernel_buffers, size_t offset, size_t count) {}

    };

    template<typename GS_0_1, typename GS_1_1, typename GS_2_1,
            typename LS_0_1, typename LS_1_1, typename LS_2_1,
            typename GS_0_2, typename GS_1_2, typename GS_2_2,
            typename LS_0_2, typename LS_1_2, typename LS_2_2,
            typename INT_RES_T,
            typename... Ts_1,
            typename... Ts_2>
    auto ocl_md_hom(const device_info&               device,

                    const kernel_info&                        kernel_1,
                    const std::tuple<Ts_1...>&                kernel_inputs_1,
                    const std::tuple<GS_0_1, GS_1_1, GS_2_1>& global_size_1,
                    const std::tuple<LS_0_1, LS_1_1, LS_2_1>& local_size_1,

                    const kernel_info&                        kernel_2,
                    const std::tuple<Ts_2...>&                kernel_inputs_2,
                    const std::tuple<GS_0_2, GS_1_2, GS_2_2>& global_size_2,
                    const std::tuple<LS_0_2, LS_1_2, LS_2_2>& local_size_2,

                    const buffer_class<INT_RES_T>&            int_res_buffer,
                    std::vector<std::string>                  wg_r_tp_names,
                    bool (*is_valid)(configuration&),

                    const size_t                              num_evaluations,
                    const size_t                              warm_ups) {
        return ocl_md_hom_wrapper_class<GS_0_1, GS_1_1, GS_2_1, LS_0_1, LS_1_1, LS_2_1,
                GS_0_2, GS_1_2, GS_2_2, LS_0_2, LS_1_2, LS_2_2,
                INT_RES_T, Ts_1..., Ts_2...>
                (device, std::tuple_cat(kernel_inputs_1, kernel_inputs_2), int_res_buffer, wg_r_tp_names, is_valid,
                 kernel_1, global_size_1, local_size_1, sizeof...(Ts_1),
                 kernel_2, global_size_2, local_size_2, sizeof...(Ts_2),
                 num_evaluations, warm_ups);
    }
}
}

#endif //MD_HOM_OCL_MD_HOM_WRAPPER_HPP
