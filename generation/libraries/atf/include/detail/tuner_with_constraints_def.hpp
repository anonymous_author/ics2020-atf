//
//  tuner_with_constraints_def.hpp
//  new_atf_lib
//
//  Created by  on 21/03/2017.
//  Copyright © 2017 . All rights reserved.
//

#ifndef tuner_with_constraints_def_h
#define tuner_with_constraints_def_h

#include <fstream>
#include <limits>

//#include "ThreadPool.h"
#include "ctpl_stl.h"

namespace atf
{


template< typename... TPs >
G_class<TPs...>::G_class( TPs&... tps )
  : _tps( tps... )
{}
  
template< typename... TPs >
auto G_class<TPs...>::tps() const
{
  return _tps;
}


template< typename abort_condition_t >
tuner_with_constraints::tuner_with_constraints( const abort_condition_t& abort_condition, const bool& abort_on_error )
  : _search_space(), _abort_on_error( abort_on_error ), _number_of_evaluated_configs(), _number_of_invalid_configs(), _evaluations_required_to_find_best_found_result(), _valid_evaluations_required_to_find_best_found_result(), _history()
{
  _abort_condition = std::unique_ptr<cond::abort>( new abort_condition_t( abort_condition ) );
  
  _history.emplace_back( std::chrono::high_resolution_clock::now(),
                         configuration{},
                         std::numeric_limits<size_t>::max()
                       );

}


// first application of operator(): IS
template< typename... Ts, typename... range_ts, typename... callables >
tuner_with_constraints& tuner_with_constraints::operator()( tp_t<Ts,range_ts,callables>&... tps )
{
  return this->operator()( G(tps...) );
}

// first application of operator(): IS (required due to ambiguity)
template< typename T, typename range_t, typename callable >
tuner_with_constraints& tuner_with_constraints::operator()( tp_t<T,range_t,callable>& tp )
{
  return this->operator()( G(tp) );
}


// second case
template< typename... Ts, typename... G_CLASSES >
tuner_with_constraints& tuner_with_constraints::operator()( G_class<Ts...> G_class, G_CLASSES... G_classes )
{
  const size_t NUM_TREEs = sizeof...(G_CLASSES) + 1;
  _search_space.append_new_trees( NUM_TREEs );
  
  insert_tp_names_in_search_space( G_class, G_classes... );
  
  return generate_config_trees< NUM_TREEs >( G_class, G_classes... );
}


template< typename callable >
configuration tuner_with_constraints::operator()( callable program ) // func must take config_t and return a value for which "<" is defined.
{
  // if no abort condition is specified then iterate over the whole search space.
  if( _abort_condition == NULL )
    _abort_condition = std::unique_ptr<cond::abort>( new cond::evaluations( _search_space.num_configs() ) );
 
 
  auto start = std::chrono::system_clock::now();

  initialize( _search_space );
  
  size_t program_runtime = std::numeric_limits<size_t>::max();
  
  // logging: cost
  std::ofstream outfile_cost;
  outfile_cost.open( "/Users//cost.csv", std::ofstream::app );
  
  // logging: meta
  std::ofstream outfile_meta;
  outfile_meta.open( "/Users//meta.csv", std::ofstream::app );
  
  while( !_abort_condition->stop( *this ) )
  {
    auto config = get_next_config();
    
    // logging: write headers
    static bool first_run = true;
    if( first_run )
    {
      // cost
      for( auto& tp : config )
        outfile_cost << tp.first << ";";
      outfile_cost << "cost" << std::endl;
      
      // meta
      outfile_meta << "number_of_evaluated_configs" << ";" << "number_of_valid_evaluated_configs" << ";" << "number_of_invalid_evaluated_configs"                                           << ";" << "evaluations_required_to_find_best_found_result" << ";" << "valid_evaluations_required_to_find_best_found_result"<< ";" << std::endl;
      
      first_run = false;
    }

    
    ++_number_of_evaluated_configs;
    try
    {
      program_runtime = program( config );
    }
    catch( ... )
    {
      ++_number_of_invalid_configs;
      
      if( _abort_on_error )
        abort();
      else
        program_runtime = std::numeric_limits<size_t>::max();
    }
    
    auto current_best_result = std::get<2>( _history.back() );
    if( program_runtime < current_best_result  )
    {
      _evaluations_required_to_find_best_found_result = this->number_of_evaluated_configs();
      _valid_evaluations_required_to_find_best_found_result = this->number_of_valid_evaluated_configs();
      _history.emplace_back( std::chrono::high_resolution_clock::now(),
                             config,
                             program_runtime
                           );
    }

    report_result( program_runtime ); // TODO: refac "program_runtime" -> "program_cost"
    
//    #ifdef VERBOSE
    std::cout << std::endl << "evaluated configs: " << this->number_of_evaluated_configs() << " , valid configs: " << this->number_of_valid_evaluated_configs() << " , program cost: " << program_runtime << " , current best result: " << this->best_measured_result() << std::endl << std::endl;
//    #endif

    // logging: cost
    for( auto& tp : config )
      outfile_cost << tp.second << ";";
    outfile_cost << program_runtime << std::endl;

  }
  
  finalize();
  
  std::cout << "\nnumber of evaluated configs: " << this->number_of_evaluated_configs() << " , number of valid configs: " << this->number_of_valid_evaluated_configs() << " , number of invalid configs: " << _number_of_invalid_configs << " , evaluations required to find best found result: " << _evaluations_required_to_find_best_found_result << " , valid evaluations required to find best found result: " << _valid_evaluations_required_to_find_best_found_result << std::endl;

  auto end = std::chrono::system_clock::now();
  auto runtime_in_sec = std::chrono::duration_cast<std::chrono::seconds>( end - start ).count();
  std::cout << std::endl << "total runtime for tuning = " << runtime_in_sec << "sec" << std::endl;
  
  
  // output
  auto        best_config = best_configuration();
  std::string seperator = "";
  std::cout << "\nbest configuration: [ ";
  for( const auto& tp : best_config )
  {
    auto tp_value = tp.second;
    std::cout << seperator << tp_value.name() << " = " << tp_value.value();
    seperator = " ; ";
  }
  std::cout << " ] with cost: " << this->best_measured_result() << std::endl << std::endl;

//  // store best found result in file
//  std::stringstream tp_names;
//  std::stringstream tp_vals;
//  for( auto& tp : best_config )
//  {
//    tp_names << tp.first  << ";";
//    tp_vals  << tp.second << ";";
//  }
  
//  std::ofstream outfile;
//  outfile.open( "results.csv", std::ofstream::app ); // TODO: "/Users//results.csv"
//  outfile << "best_measured_result" << ";" << "number_of_valid_evaluated_configs" << ";" << "evaluations_required_to_find_best_found_result" << ";" << "valid_evaluations_required_to_find_best_found_result" ";" << tp_names.str() << std::endl;
//  outfile << this->best_measured_result() << ";" << this->number_of_valid_evaluated_configs() << ";" << _evaluations_required_to_find_best_found_result << ";" << _valid_evaluations_required_to_find_best_found_result << ";" << tp_vals.str() << std::endl;
//  outfile.close();

//  // logging: cost
//  std::ofstream outfile_cost;
//  outfile_cost.open( "/Users//cost.csv", std::ofstream::app );
//  outfile_cost << tp_names.str() << ";" << "cost"                       << std::endl;
//  outfile_cost << tp_vals.str()  << ";" << this->best_measured_result() << std::endl;
//  outfile_cost.close();

  // logging: cost
  outfile_cost.close();
  
  // logging: meta
  outfile_meta << this->number_of_evaluated_configs() << ";" << this->number_of_valid_evaluated_configs() << ";" << this->number_of_evaluated_configs() - this->number_of_valid_evaluated_configs() << ";" << _evaluations_required_to_find_best_found_result  << ";" << _valid_evaluations_required_to_find_best_found_result << ";" << std::endl;
  outfile_meta.close();


  auto best_configuration = std::get<1>( _history.back() );
  return best_configuration;
}


template< typename... Ts, typename... rest_tp_tuples >
void tuner_with_constraints::insert_tp_names_in_search_space( G_class<Ts...> tp_tuple, rest_tp_tuples... tuples )
{
  insert_tp_names_of_one_tree_in_search_space ( tp_tuple, std::make_index_sequence<sizeof...(Ts)>{} );
  
  insert_tp_names_in_search_space( tuples... );
}


template< typename... Ts, size_t... Is>
void tuner_with_constraints::insert_tp_names_of_one_tree_in_search_space ( G_class<Ts...> tp_tuple, std::index_sequence<Is...> ) // TODO refac: insert_tp_names_of_one_tree_in_search_space  -> insert_tp_names_of_one_tree_in_search_space
{
  insert_tp_names_of_one_tree_in_search_space ( std::get<Is>( tp_tuple.tps() )... );
}

template< typename T, typename range_t, typename callable, typename... Ts >
void tuner_with_constraints::insert_tp_names_of_one_tree_in_search_space ( tp_t<T,range_t,callable>& tp, Ts&... tps )
{
  _search_space.add_name( tp.name() );
  
  insert_tp_names_of_one_tree_in_search_space ( tps... );
}


template< size_t TREE_ID, typename... Ts, typename... rest_tp_tuples>
tuner_with_constraints& tuner_with_constraints::generate_config_trees( G_class<Ts...> tp_tuple, rest_tp_tuples... tuples )
{
  return generate_config_trees<TREE_ID>( tp_tuple, std::make_index_sequence<sizeof...(Ts)>{}, tuples... ); //TODO: vereinfachen?: "std::make_index_sequence<sizeof...(Ts)>{}" -> "sizeof...(Ts)"
}


template< size_t TREE_ID, typename... Ts, size_t... PARAM_IDs, typename... rest_tp_tuples >
tuner_with_constraints& tuner_with_constraints::generate_config_trees( G_class<Ts...> tp_tuple, std::index_sequence<PARAM_IDs...>, rest_tp_tuples... tuples )
{
  assert( TREE_ID == sizeof...(rest_tp_tuples) + 1 );
  
  // fill generated config tree
  auto&          tree      = _search_space.tree( _search_space.num_trees() - TREE_ID ); // Note: TREE_ID is offset starting from right
  tp_value_node& tree_root = tree.root_non_const();
  
  const size_t NUM_TPs_TO_PROCESS = sizeof...(PARAM_IDs);
  static_assert( TREE_LAYERS_TO_PARALLELIZE <= NUM_TPs_TO_PROCESS , "TREE_LAYERS_TO_PARALLELIZE has to be smaller or equal than NUM_TPs_TO_PROCESS\n");
  
  tree.set_depth( NUM_TPs_TO_PROCESS );
  
  auto& num_tree_leafs = tree.num_leafs();
  _threads.emplace_back( [ =, &tree_root, &num_tree_leafs ]() mutable
                                          {
                                            generate_single_config_tree_in_parallel< TREE_LAYERS_TO_PARALLELIZE, NUM_TPs_TO_PROCESS, TREE_ID >
                                            (
                                              tree_root,
                                              num_tree_leafs,
                                              std::make_index_sequence< 0 >{},  // Note: initially no tuning parameter values are passed
                                              std::get< PARAM_IDs >( tp_tuple.tps() )...
                                            );
                                          }
                       );
  
  return generate_config_trees< TREE_ID-1 >( tuples... );
}


template< size_t TREE_ID >
tuner_with_constraints& tuner_with_constraints::generate_config_trees()
{
  for( auto& thread : _threads )
    thread.join();
  
  _threads.clear();

  return *this;
}


template< size_t NUM_TREE_LAYERS_TO_PARALLELIZE, size_t NUM_TPs_TO_PROCESS, size_t TREE_ID, size_t... TP_VALUE_IDs, typename T, typename range_t, typename callable, typename... Ts, std::enable_if_t<( NUM_TREE_LAYERS_TO_PARALLELIZE > 1 )>* >
void tuner_with_constraints::generate_single_config_tree_in_parallel( tp_value_node& tree_root, size_t& num_tree_leafs, std::index_sequence<TP_VALUE_IDs...>, tp_t<T,range_t,callable>& tp, Ts&... tps )
{
//  static_assert( NUM_TPs_TO_PROCESS + sizeof...(TP_VALUE_IDs) == sizeof...(tps) + 1, "error" ); // Note: tps also contains tuning parameter values (and not only tuning parameters)
  
  T value;
  
  while( tp.get_next_value(value) )
    if( tp.get_constraint()( std::get< NUM_TPs_TO_PROCESS-1 + TP_VALUE_IDs >( std::make_tuple(tps...) )..., value ) ) // Note: NUM_TPs_TO_PROCESS is offset to tuning parameter values
    {
      auto& inserted_node = tree_root.insert( value );
      
      generate_single_config_tree_in_parallel< NUM_TREE_LAYERS_TO_PARALLELIZE-1, NUM_TPs_TO_PROCESS-1, TREE_ID >( inserted_node,
                                                                                                          num_tree_leafs,
                                                                                                          std::make_index_sequence< sizeof...(TP_VALUE_IDs)+1 >{},
                                                                                                          tps...,
                                                                                                          value
                                                                                                        );
      
    }
}


// initial with no TP_VALUE_IDs, i.e, NUM_TREE_LAYERS_TO_PARALLELIZE was initially == 1
template< size_t NUM_TREE_LAYERS_TO_PARALLELIZE, size_t NUM_TPs_TO_PROCESS, size_t TREE_ID, size_t... TP_VALUE_IDs, typename T, typename range_t, typename callable, typename... Ts, std::enable_if_t<( NUM_TREE_LAYERS_TO_PARALLELIZE == 1 )>* >
void tuner_with_constraints::generate_single_config_tree_in_parallel( tp_value_node& tree_root, size_t& num_tree_leafs, std::index_sequence< TP_VALUE_IDs... >, tp_t<T,range_t,callable>& tp, Ts&... tps )
{
//  static_assert( NUM_TPs_TO_PROCESS + sizeof...(TP_VALUE_IDs) == sizeof...(tps) + 1, "error" ); // Note: tps also contains tuning parameter values (and not only tuning parameters)
  
  std::vector< std::unique_ptr<size_t> > num_sub_trees_leafs;

  ctpl::thread_pool p( NUM_THREADS_SP_GEN /* NUM_THREADS_SP_GEN many threads in the pool */);
  
  std::vector< std::function<void(int)> > lambdas;
  std::vector< std::future<void> >        futures;

  T value;
  
  // start threads
  while( tp.get_next_value(value) )
    if( tp.get_constraint()( std::get< NUM_TPs_TO_PROCESS-1 + TP_VALUE_IDs >( std::make_tuple(tps...) )..., value ) ) // Note: NUM_TPs_TO_PROCESS is offset to tuning parameter values
    {
      auto& inserted_node = tree_root.insert( value );
    
      auto& num_sub_tree_leafs = (num_sub_trees_leafs.emplace_back( std::make_unique<size_t>(0) ), *num_sub_trees_leafs.back() );
      
      auto generate_single_config_tree_as_lambda = [ this, &num_tree_leafs, &inserted_node, &num_sub_tree_leafs, tps..., value ]( auto ) mutable
                                                   {
                                                     // if parallelization in all tree layers, and flag ENABLE_ACCELERATION_FOR_SMALL_SEARCH_SPACES is enabled, then also insert "inserted_node" as leaf
                                                     if( NUM_TPs_TO_PROCESS == 1 )
                                                     {
                                                       #ifdef ENABLE_ACCELERATION_FOR_SMALL_SEARCH_SPACES
                                                         _search_space.tree( _search_space.num_trees() -  TREE_ID ).insert_leaf( inserted_node ); // read TREE_IDs from right to left!
                                                       #endif
                                                       
                                                       //++num_tree_leafs;
                                                       _search_space.tree( _search_space.num_trees() -  TREE_ID ).atomic_inc_num_leafs();
                                                       
                                                       return;
                                                     }
                                                    
                                                     this->generate_single_config_tree< NUM_TPs_TO_PROCESS-1, TREE_ID >
                                                     (
                                                       inserted_node,
                                                       num_sub_tree_leafs,
                                                       std::make_index_sequence< sizeof...(TP_VALUE_IDs)+1 >{},
                                                       tps...,
                                                       value
                                                     );
                                                   };
      lambdas.push_back( generate_single_config_tree_as_lambda );
      
      
      futures.push_back( p.push( lambdas.back() ) );
    }

  // wait for threads
  for( auto& future : futures )
    future.wait();
  
  // summation over sub tree sizes
  for( const auto& num_sub_tree_leafs : num_sub_trees_leafs )
    num_tree_leafs += *num_sub_tree_leafs;

  // TODO: delete incomplete paths with NUM_TREE_LAYERS_TO_PARALLELIZE many nodes
//  for( size_t i = 0 ; i < num_sub_trees_leafs.size() ; ++i )
//    if( *(num_sub_trees_leafs[i]) == 0 )
//      std::cout << "NOTE: incomplete path found at position " << i <<". Please set penalty value!\n";
//  for( size_t i = 0 ; i < num_sub_trees_leafs.size() ; ++i )
//  {
//    num_tree_leafs += *num_sub_tree_leafs;
//    
//    // delete invalid paths
//    if( *num_sub_tree_leafs[i] == 0 )
//    {
//      
//    }
//  }
}




template< size_t NUM_TREE_LAYERS_TO_PARALLELIZE, size_t NUM_TPs_TO_PROCESS, size_t TREE_ID, size_t... TP_VALUE_IDs, typename... Ts, std::enable_if_t<( NUM_TREE_LAYERS_TO_PARALLELIZE == 0 )>* >
void tuner_with_constraints::generate_single_config_tree_in_parallel( tp_value_node& tree_root, size_t& num_tree_leafs, std::index_sequence<TP_VALUE_IDs...>, Ts&... tps )
{
//  static_assert( NUM_TPs_TO_PROCESS + sizeof...(TP_VALUE_IDs) == sizeof...(tps), "error" ); // Note: tps also contains tuning parameter values (and not only tuning parameters)
  
  generate_single_config_tree< NUM_TPs_TO_PROCESS, TREE_ID >( tree_root, num_tree_leafs, std::make_index_sequence< sizeof...(TP_VALUE_IDs) >{},tps... );
}


template< size_t NUM_TPs_TO_PROCESS, size_t TREE_ID, size_t... TP_VALUE_IDs, typename T, typename range_t, typename callable, typename... Ts, std::enable_if_t<( NUM_TPs_TO_PROCESS > 0 )>* >
void tuner_with_constraints::generate_single_config_tree( tp_value_node& sub_tree_root, size_t& num_sub_tree_leafs, std::index_sequence<TP_VALUE_IDs...>, tp_t<T,range_t,callable>& tp, Ts&... tps )
{
//  static_assert( NUM_TPs_TO_PROCESS + sizeof...(TP_VALUE_IDs) == sizeof...(tps) + 1 , "error" ); // Note: tps also contains tuning parameter values (and not only tuning parameters)
  
  T value;
  while( tp.get_next_value(value) )
    if( tp.get_constraint()( std::get< NUM_TPs_TO_PROCESS-1 + TP_VALUE_IDs >( std::make_tuple(tps...) )..., value ) ) // Note: NUM_TPs_TO_PROCESS is offset to tuning parameter values
      generate_single_config_tree< NUM_TPs_TO_PROCESS-1, TREE_ID >( sub_tree_root, num_sub_tree_leafs, std::make_index_sequence< sizeof...(TP_VALUE_IDs)+1 >{}, tps..., value );
}


template< size_t NUM_TPs_TO_PROCESS, size_t TREE_ID, size_t... TP_VALUE_IDs, typename... Ts, std::enable_if_t<( NUM_TPs_TO_PROCESS == 0 )>* >
void tuner_with_constraints::generate_single_config_tree( tp_value_node& sub_tree_root, size_t& num_sub_tree_leafs, std::index_sequence<TP_VALUE_IDs...>, Ts&... tp_values )
{
//  static_assert( NUM_TPs_TO_PROCESS + sizeof...(TP_VALUE_IDs) == sizeof...(tp_values) + 1, "error" ); // Note: tps also contains tuning parameter values (and not only tuning parameters)
  
  const size_t OFFSET_TO_VALUES_NOT_GENERATED_IN_PARALLEL = sizeof...( Ts ) - sizeof...( TP_VALUE_IDs );
  auto& inserted_node = sub_tree_root.insert( std::get< OFFSET_TO_VALUES_NOT_GENERATED_IN_PARALLEL + TP_VALUE_IDs >( std::make_tuple(tp_values...) )... ); (void) inserted_node; // Note: silences warning "unused variable"
  
  //tp_values... ); (void) inserted_node; // Note: silences warning "unused variable"
  
  ++num_sub_tree_leafs;
  //_search_space.tree( _search_space.num_trees() -  TREE_ID ).inc_num_leafs();
  
  // when flag ENABLE_ACCELERATION_FOR_SMALL_SEARCH_SPACES is enabled, also insert "inserted_node" as leaf
  #ifdef ENABLE_ACCELERATION_FOR_SMALL_SEARCH_SPACES
    _search_space.tree( _search_space.num_trees() -  TREE_ID ).insert_leaf( inserted_node ); // read TREE_IDs from right to left! (it is easier to decrement TREE_ID to 0 than increment 0 to TREE_ID)
  #endif
}


//// required when all tree layers are parallelized
//template< size_t TREE_ID >
//void tuner_with_constraints::generate_single_config_tree( tp_value_node& sub_tree_root )
//{
//}


// TODO: loeschen
template< typename T, typename... Ts >
void tuner_with_constraints::print_path(T val, Ts... tps)
{
  std::cout << val << " ";
  print_path(tps...);
}


} // namespace "atf"

#endif /* tuner_with_constraints_def_h */
