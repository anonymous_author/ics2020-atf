# Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters

This artifact contains the workflow to reproduce the results shown in the paper *Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters* submitted to the [34th ACM International Conference on Supercomputing (ICS-2020)](https://ics2020.bsc.es/). The user is invited to perform the steps described below. In case of **any problems**, please feel free to **open an issue** in order to get in contact with the authors.

## Software Requirements

- an OpenCL 1.2 driver and runtime environment
- CMake 3.8 or higher
- a compiler supporting C++14 or higher (tested with GCC 8.2.0)
- Python 2.7 (not Python 3.x)
- OpenTuner 0.8.0
- jq

## Experiment workflow

The user is invited to perform the following steps:
1. Download the artifact repository:
    
    `$ git clone https://gitlab.com/anonymous_author/ics2020-atf.git`
2. Change into artifact's root directory:

    `$ cd ics2020-atf`

    Set the environment variable `ARTIFACT_DIR` to artifact's root directory; for this, execute

    ```
    $ export ARTIFACT_DIR=`pwd`
    ```

    inside the artifact's directory.
       
4. Compile artifact's source code:

    `$ ./scripts/install.sh`

    All arguments provided to script `install.sh` will be directly forwarded to CMake when building the binaries. For example, forwarding arguments can be required if some dependencies cannot be found by CMake (e.g., when dependencies are not installed in their default locations).

5. Select the desired OpenCL platform and device id by editing file `config.json` in line 2 and 3; available platforms and devices, as well as their corresponding ids, can be listed by executing command `clinfo`.
6. Run our experiments:
    
    `$ ./scripts/run_all_experiments.sh`
7. Run the references:
    
    `$ ./scripts/run_all_references.sh`
8. Plot the graphs:
    
    `$ ./scripts/plot_graphs.sh`

__Note:__ When changing the platform or device id in file `config.json`, make sure to clean previous results by executing `$ ./scripts/clean_results.sh`.

